<li class="nav-item">
    <a href="{{ route('dosen.view') }}" class="nav-link text-light {{ $title === 'Kegiatan' ? 'active' : '' }}">
        <span style="margin-right: 3px"><i class="bi bi-calendar-event"></i></span>
        Kegiatan
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('dosen.laporan.index') }}" class="nav-link text-light {{ $title === 'Laporan' ? 'active' : '' }}">
        <span style="margin-right: 3px;"><i class="bi bi-journal-text"></i></span>
        Laporan
    </a>
</li>

