<style>
    .dropdown-menu {
        left: auto;
        /* Reset nilai left */
        right: 0;
        /* Geser dropdown ke kanan */
    }
</style>

<section id="admin-navbar">
    <section id="navbar">
        <nav class="navbar navbar-expand-lg navbar-light pt-3 pb-3 px-3 d-flex justify-content-end"
            style="background-color: #f76707">
            <div class="dropdown">
                <img src="{{ asset('avatars/' . (Auth::user()->avatar ?? 'default.png')) }}" alt="Profile Image"
                    id="profileImage" class="rounded-circle" height="45" width="45" data-toggle="dropdown">
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('admin.profile') }}"><i data-feather="user"></i>Profile</a>
                    <form action="{{ route('logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="dropdown-item"><i data-feather="log-out"></i> Logout</button>
                    </form>
                </div>
            </div>
        </nav>
    </section>
</section>
